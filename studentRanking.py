def isGreater(student1, student2):      #student1 >= student2 -> true
    for i in range(1,len(student1)):
        if (int(student1[i]) < int(student2[i])):
            return False;
    return True;


def studentList(fhand):
    students = []
    for line in fhand:
        student = line.strip().split()
        students.append(student)
    return students;

def getRelationships(students):
    relationships = dict()
 
    for i in range(0, len(students)-1):
        for j in range(i+1, len(students)):
            if(isGreater(students[i], students[j])):
                if students[i][0] not in relationships:
                    relationships[students[i][0]] = [students[j][0]];
                else:
                    relationships[students[i][0]].append(students[j][0]);
    return relationships;


def removeTransitivity(relations):
    for student in relations:
        for substudent in relations[student]:
            if substudent in relations.keys():
                for subsubstudent in relations[substudent]:
                    if subsubstudent in relations[student]:
                        relations[student].remove(subsubstudent)
    return relations;

def main():
    fhand = open('studentRanking.txt','r')
    students = studentList(fhand);
    relations = getRelationships(students);
    relations = removeTransitivity(relations) 

    for student in relations:
        for substudent in relations[student]:
            print (student,">", substudent)    

if __name__=="__main__":
    main()
